import { BrowserRouter as Router, Routes, Route } from "react-router-dom"
import { useState, useEffect } from "react"
import Navbar from "./scenes/global/Navbar"
import Home from "./scenes/home/Home"
import NotFound from "./scenes/notfound/NotFound"
import "bootstrap/dist/css/bootstrap.min.css"
import { useGlobalContext } from "./state/index"
import CartModal from "./scenes/global/CartModal"
import Footer from "./scenes/global/Footer"
import ShoppingList from "./scenes/home/ShoppingList"
import CreateUpdateProductModal from "./scenes/admin/CreateUpdateProductModal"
import ChangePassword from "./scenes/global/ChangePassword"
import ItemPage from "./scenes/home/ItemPage"
import OrderPage from "./scenes/home/OrderPage"
import ProductSettings from "./scenes/admin/ProductSettings"
import UsersSettings from "./scenes/admin/UsersSettings"
import Orders from "./scenes/admin/Orders"
 


function App() {  
  const { showModal, showModalProduct, setUser, user, showChangePassword, setIsLoggedIn } = useGlobalContext()
  const [selectedPage, setSelectedPage ] = useState("home");

  useEffect(() => {
    const persistLogin = localStorage.getItem("token")
    if(persistLogin) {
      setIsLoggedIn(true)
      setUser({
        id: localStorage.getItem("id"),
        token: localStorage.getItem("token"),
        isAdmin: JSON.parse(localStorage.getItem("isAdmin"))
      })

    } 
  },[])



  
  return (      
    <div className="app">
      <Router>
        <Navbar 
          selectedPage={selectedPage} 
          setSelectedPage={setSelectedPage}
        />
        {showModal && <CartModal />}
        {showModalProduct && <CreateUpdateProductModal />}
        {showChangePassword && <ChangePassword />}
        <Routes>          
          <Route exact path="/" element={<Home />} />
          { (user.isAdmin === true && user !== null ) ? (
            <>
              <Route path="/productsettings" element={<ProductSettings />} />
              <Route path="/userssettings" element={<UsersSettings />} />
              <Route path="/orders" element={<Orders />} />
            </>            
          ) : (
            <>
              <Route path="/products" element={<ShoppingList />} />
              <Route path="/item" element={<ItemPage />} />
              <Route path="/order" element={<OrderPage />} />
            </>
          )}
          <Route path="/*" element={<NotFound />} />
        </Routes>
               
      </Router>
    </div>
  )
}

export default App

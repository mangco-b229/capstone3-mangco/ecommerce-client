import { Card, Button } from "react-bootstrap"
import { Link, useNavigate } from "react-router-dom" 
import { useState, useEffect } from "react"
import Swal from "sweetalert2"
import { useGlobalContext } from "../state/index"
import { BiMinusCircle, BiPlusCircle } from "react-icons/bi"


const Item = ({ productProps }) => {
	const { _id , name, description, category, price } = productProps
	const [ isDisabled, setIsDisabled ] = useState(true)
	const { user, isLoggedIn, setSelectedProductId, setCartProducts, cartProducts, setCartCount, cartCount } = useGlobalContext() 
	const [count ,setCount] = useState(1)

	const navigate = useNavigate()

	useEffect(() => {
		if(!isLoggedIn) {
			setIsDisabled(true)
		} else {
			setIsDisabled(false)
		}

	}, [isLoggedIn])

	const handleImageClicked = () => {
		setSelectedProductId(_id)
		navigate('/item')
	}

	const addToCart = async () => {
		const response = await fetch(`${import.meta.env.VITE_ECOM}/carts/${user.id}`, {
			method: "POST",
			body: JSON.stringify({
				productId: _id,
				quantity: count,
			}),
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()
		Swal.fire({
			position: 'top-end',
			timer: 1500,
			showConfirmButton: false,
			color: '#8f7840',
			background: '#ecebeb',
	    	title: "Success",
	    	icon: "success",
	    	text: `${name} successfully added to cart` 
	    })
	    setCartCount(cartCount + count)
	}

	return (
		<div className="card-container">
			<Card className="cardHighlight">
				<Card.Img className="product-image" onClick={handleImageClicked} variant="top" src={`../assets/products/${_id}.png`} />
				<Card.Body className="d-flex flex-column justify-content-between">
					<div className="d-flex justify-content-between align-items-center flex-row">
						<Card.Title className="d-flex">{name}</Card.Title>
						<Card.Title>Php {price}</Card.Title>
						
					</div>
					<div className="d-flex flex-row justify-content-between align-items-end">
						<div className="d-flex count-buttons">
							<BiPlusCircle onClick={() => setCount(count + 1)} className="plus-count btn-icon" size={36}/>			
							{count}
							<BiMinusCircle onClick={() => count > 1 && 	setCount(count - 1)} className="plus-count btn-icon" size={36}/>	
						</div>
						<div className="cart-error-container">
							{ !isLoggedIn ? <p className="cart-error">*please login</p> : null}
							<Button className="btn-addToCart btn-secondary" onClick={addToCart} disabled={isDisabled}>
								Add To Cart
							</Button>
						</div>
					</div>
				</Card.Body>
			</Card>
		</div>
	)
}

export default Item
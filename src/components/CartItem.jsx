import { BiMinusCircle, BiPlusCircle } from "react-icons/bi"
import {useState, useEffect, useMemo } from 'react'
import { useGlobalContext } from "../state/index"

const CartItem = ({ itemProps }) => {
	const { user, cartProducts, setCartProducts, setCartCount } = useGlobalContext()
	const { productId, name, quantity, price, subtotal } = itemProps
	const [ count ,setCount] = useState(quantity)
	 	
	const removeFromCart = useMemo(() => {
    return async () => {
      const response = await fetch(`${import.meta.env.VITE_ECOM}/carts/${user.id}/${productId}`, {
        method: "DELETE",
        headers: {
          Authorization: `Bearer ${user.token}`,
          'Content-Type': 'application/json'
        }
      })

      const data = await response.json()

      setCartProducts((prevCart) => {
        return prevCart.filter((item) => item.productId !== productId);
      })
      setCartCount(count)
    }
  }, [user.id, user.token, productId, setCartProducts])

	const updateCartQty = useMemo(() => {
    return async () => {
      const response = await fetch(`${import.meta.env.VITE_ECOM}/carts/${user.id}/${productId}`, {
        method: "PUT",
        headers: {
          Authorization: `Bearer ${user.token}`,
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          productId: productId,
          quantity: count
        })
      })
      setCartCount(count)

      const data = await response.json()
    }
  }, [count, user.id, user.token, productId])

	useEffect(() => {
		updateCartQty()
	}, [count])

	return (
		<div className="mb-2">
			<div className="cart-item">
				<div className="d-flex flex-row justify-content-between">
					<p>{name}</p>
					<p className="btn-icon" onClick={removeFromCart}>REMOVE</p>
				</div>
				<p className="price">Price: Php {price}</p>
				<div className="d-flex flex-row justify-content-between align-items-center">
					<div className="d-flex align-items-center count-buttons">
							<BiPlusCircle onClick={() => setCount(count + 1)} className="plus-count btn-icon" size={36}/>			
							{count}
							<BiMinusCircle onClick={() => count > 1 && 	setCount(count - 1)} className="plus-count btn-icon" size={36}/>	
					</div>
					<p className="p-0 m-0">Subt: Php {subtotal}</p>
				</div>
			</div>
		</div>
	)
}

export default CartItem
import { useContext, useState, useEffect } from "react"
import {AiOutlineClose} from "react-icons/ai"
import { Button } from "react-bootstrap"
import { useGlobalContext } from "../../state/index"
import CartItem from "../../components/CartItem"
import { useNavigate } from "react-router-dom" 
import PropagateLoader from 'react-spinners/PropagateLoader'

import Swal from "sweetalert2"



const CartModal = () => {

	const { cartCount, closeModal, user, setCartCount, setCartProducts, cartProducts } = useGlobalContext()
	const [products, setProducts] = useState([])
	const [ totalAmount, setTotalAmount ] = useState()
	const navigate = useNavigate()

	
	const fetchProducts = async () => {
		const response = await fetch(`${import.meta.env.VITE_ECOM}/carts/${user.id}/cart`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})
		const data =  await response.json()

		setProducts(data[0].products.map(({ _id, productId, name, quantity, price, subtotal }) => ({
	    _id,
	    productId,
	    name,
	    quantity,
	    price,
	    subtotal
	  })))

		setTotalAmount(data[0].totalAmount)
		setCartProducts(data[0].products)		

		const totalQuantity = getTotalQuantity(data[0].products);
		setCartCount(totalQuantity)
	}

	//gets the total quantity inside the cart
	const getTotalQuantity = (products) => {
	  let totalQuantity = 0;
	  products.forEach(product => {
	    totalQuantity += product.quantity;
	  });
	  return totalQuantity;
	}

	
	useEffect(() => {  	 
  	fetchProducts()
	}, [cartCount])

  const createOrder = async () => {
  	await fetchProducts()

  	const formattedProducts = cartProducts.map(
  		({ productId, quantity }) => {
  			return { productId, quantity }
  		}
		)

  	const response = await fetch(`${import.meta.env.VITE_ECOM}/orders/${user.id}`, {
  		method: "POST",
  		body: JSON.stringify({ products: formattedProducts }),
  		headers: {
  			Authorization: `Bearer ${user.token}`,
  			'Content-Type': 'application/json'
  		}
  	})

  	const data = await response.json()
  }


  const handlePlaceOrderClicked = () => {
  	{ cartProducts && 
			Swal.fire({
			  title: 'Confirm Order',
			  text: "Once you click Confirm the Order will be placed",
			  icon: 'warning',
			  showCancelButton: true,
			  background: '#ecebeb',
			  confirmButtonColor: '#8f7840',
			  cancelButtonColor: '#d33',
			  confirmButtonText: 'Confirm'
			}).then((result) => {
			  if (result.isConfirmed) {
			  	createOrder()
			  	closeModal()
			  	navigate('/products')
			    Swal.fire(
			      'Order Placed!',
			      'Thank you for shopping!',
			      'success'		      
			    )
			  }
			})
  	}
	    	
  }



	return (
		<aside className="modal-overlay">
			<div className="modal-container p-3">
				<div className="mb-4 d-flex flex-row justify-content-between align-items-center">
					<h4 className="p-0 m-0">Shopping Cart</h4>
					<div onClick={closeModal} className="close-modal btn-icon" >
						<AiOutlineClose />
					</div>
				</div>
				<div>
					 {products.map(({ _id, productId, name, quantity, price, subtotal }) => (
			      <div key={_id}>
			        <CartItem itemProps={{ _id, productId, name, quantity, price, subtotal }} />
			      </div>
			    ))}
				</div>

				<div className="d-flex flex-row justify-content-between align-items-center p-2">
					<p className="fw-bold">Total Amount</p>
					<p className="fw-bold">Php {totalAmount}</p>
				</div>
				<div className="d-flex flex-column">
					<Button className="btn-login btn-secondary" onClick={handlePlaceOrderClicked} block>Place Order</Button>
				</div>

			</div>
		</aside>
	)
}

export default CartModal
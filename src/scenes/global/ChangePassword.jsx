import { useGlobalContext } from "../../state/index"
import {AiOutlineClose} from "react-icons/ai"
import { Form, Button, FloatingLabel } from "react-bootstrap"
import { useState, useEffect } from "react"
import Swal from "sweetalert2"

const ChangePassword = () => {
	const {user, closePass} = useGlobalContext()
	const [existingPass, setExistingPass] = useState("")
	const [newPass, setNewPass] = useState("")
	const [ verifyNewPass, setVerifyNewPass] = useState("")
	const [ error, setError ] = useState("")
	const [ isButtonPassDisabled, setIsButtonPassDisabled] = useState(true)

	const changePassword = async (e) => {
		e.preventDefault()

		const response = await fetch(`${import.meta.env.VITE_ECOM}/users/${user.id}/changepassword`, {
			method: 'PUT',
			body: JSON.stringify({
				existingPassword: existingPass,
				newPassword: newPass
			}),
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()
		
		if(data) {
			Swal.fire({
				confirmButtonColor: '#8f7840',
				color: '#8f7840',
				background: '#ecebeb',
        title: "Success",
        icon: "success",
        text: `Your password has been changed!`
      })
      closePass()
		} else {
			Swal.fire({
				confirmButtonColor: '#8f7840',
				color: '#8f7840',
				background: '#ecebeb',
        title: "Incorrect Password Error",
        icon: "error",
        text: `We're unable to recognize the password you entered. Could you please double check and try again?`
      })		
		}
		setExistingPass("")
		setNewPass("")
		setVerifyNewPass("")
	}

	useEffect(() => {
		setError(newPass !== verifyNewPass ? "New Password and Verifiy Password do not match" : "")
		setIsButtonPassDisabled(!existingPass || !newPass || !verifyNewPass || newPass !== verifyNewPass)
	},[existingPass, newPass, verifyNewPass])

	useEffect(() => {
		setError("")
	}, [existingPass])

	const handleChangePassword = async (e) => {
		changePassword(e)
	} 

	const handleExistingInput = (e) => setExistingPass(e.target.value)
	const handleNewPassInput = (e) => setNewPass(e.target.value)
	const handleVerifyNewPass = (e) => setVerifyNewPass(e.target.value)

	const errClass = error ? "error-msg" : "offscreen"

	return (
		<aside className="modal-overlay-changepass">
			<div className="modal-container-changepass p-2 d-flex flex-column justify-content-center">
				<div className="h-75 d-flex flex-row d-flex flex-column w-100">
					<div className="w-100 h-100 p-3 pb-0">
						<div className="d-flex flex-row justify-content-between">
							<h4 className="mb-4">User Info:</h4>
							<div onClick={closePass} className="close-modal btn-icon" >
								<AiOutlineClose />
							</div>
						</div>
						<p>User ID : {user.id}</p>
						<p>Email : {localStorage.getItem('email')}</p>
						<p>isAdmin : {localStorage.getItem('isAdmin')}</p>
					</div>
					<div className="h-100 p-3 pt-1">
				    <h4 className="mb-3">Change Password:</h4>
						<Form onSubmit={handleChangePassword}>
							<Form.Group controlId="formBasicEmail" className="pb-3">					            					
	              <p className={errClass}>{error}</p>
	              <FloatingLabel
	              	controlId="floatingName"
									label="Existing Password"
	              >
    		        	<Form.Control 
    		        		type="Password" 
    		        		placeholder="Existing Password" 
    		        		name="Password" 
    		        		value={existingPass} 
    		        		onChange={handleExistingInput} 
    		        		required
    		        	
    		        	/>
    		        </FloatingLabel>		        
	            </Form.Group>
	            <Form.Group controlId="formBasicEmail" className="pb-3">					            					
	              <FloatingLabel
	              	controlId="floatingName"
									label="New Password"
	              >
    		        	<Form.Control 
    		        		type="Password" 
    		        		placeholder="New Password" 
    		        		name="Password" 
    		        		value={newPass} 
    		        		onChange={handleNewPassInput} 
    		        		required
    		        	
    		        	/>
    		        </FloatingLabel>		        
	            </Form.Group>
	            <Form.Group controlId="formBasicEmail" className="pb-3">					            					
	              <FloatingLabel
	              	controlId="floatingName"
									label="Confirm New Password"
	              >
    		        	<Form.Control 
    		        		type="Password" 
    		        		placeholder="Confirm New Password" 
    		        		name="Password" 
    		        		value={verifyNewPass} 
    		        		onChange={handleVerifyNewPass} 
    		        		required
    		        	
    		        	/>
    		        </FloatingLabel>		        
	            </Form.Group>
	            <div className="buttons-container">
								<Button type="submit" className="btn-login btn-secondary" disabled={isButtonPassDisabled}>Change Password</Button>
								<Button 
									onClick={closePass} 
									className="btn-danger"
								>Cancel</Button>
							</div>
						</Form>
					</div>
				</div>
			</div>
		</aside>
	)
}

export default ChangePassword
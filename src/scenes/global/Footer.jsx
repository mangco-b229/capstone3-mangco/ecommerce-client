import { Container, Card, Row, Col } from "react-bootstrap"


const Footer = () => {

	const currentYear = new Date().getFullYear();

	return (
		<div className="d-block">
	    <div className="footer">
	    	<Container className="mt-3 p-2">
	    	  <Row className="justify-content-between" style={{ flexWrap: "wrap" }}>
	    	    <Col xs="12" sm="6" md="3">
	    	      <Card className="card-custom">
	    	        <Card.Body className="card-body-footer">
	    	          <Card.Title className="fw-bold">
	    	            Knives Out
	    	          </Card.Title>
	    	          <p>
	    	            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
	    	            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
	    	            ad minim veniam,
	    	          </p>
	    	        </Card.Body>
	    	      </Card>
	    	    </Col>
	    	    <Col xs="12" sm="6" md="3">
	    	      <Card className="card-custom">
	    	        <Card.Body className="card-body-footer">
	    	          <Card.Title className="fw-bold">
	    	            About Us
	    	          </Card.Title>
	    	          <p>Careers</p>
	    	          <p>Our Stores</p>
	    	          <p>Terms & Conditions</p>
	    	          <p>Privacy Policy</p>
	    	        </Card.Body>
	    	      </Card>
	    	    </Col>
	    	    <Col xs="12" sm="6" md="3">
	    	      <Card className="card-custom">
	    	        <Card.Body className="card-body-footer">
	    	          <Card.Title className="fw-bold">
	    	            Customer Care
	    	          </Card.Title>
	    	          <p>Help Center</p>
	    	          <p>Track Your Order</p>
	    	          <p>Corporate & Bulk Purchasing</p>
	    	          <p>Returns & Refunds</p>
	    	        </Card.Body>
	    	      </Card>
	    	    </Col>
	    	    <Col xs="12" sm="6" md="3">
	    	      <Card className="card-custom">
	    	        <Card.Body className="card-body-footer">
	    	          <Card.Title className="fw-bold">
	    	            Contact Us
	    	          </Card.Title>
	    	          <p>50 north Whatever Blvd, Washington, DC 10501</p>
	    	          <p>Email: dummy@gmail.com</p>
	    	          <p>(222)333-4444</p>
	    	        </Card.Body>
	    	      </Card>
	    	    </Col>
	    	  </Row>
	    	</Container>
	    </div>
	    <div className="all-rights text-center text-dark d-flex align-items-center justify-content-center">Copyright &copy; {currentYear} All Rights Reserved</div>
    </div>
  );
}

export default Footer
import Item from "../../components/Item"
import { useState, useEffect } from "react"
import { Row, Col, Button, Tab } from "react-bootstrap"
import { useParams } from "react-router-dom"
import PropagateLoader from 'react-spinners/PropagateLoader'
import Footer from "../global/Footer"


const ShoppingList = ({setSelectedPage}) => { 
	const [products, setProducts] = useState([])
	const [loading, setLoading] = useState(true)
	const [value, setValue] = useState("all")

	const fetchProducts = async () => {
		setLoading(true)

		const res = await fetch(`${import.meta.env.VITE_ECOM}/products/all`)
		const data = await res.json()

		/*setProducts(data.map(prod => (
			<Col key={prod._id} xs={12} md={6} lg={4}>
				<Item productProps={prod}/>
			</Col>
		)))*/

		setProducts(data)
		setLoading(false)
	}

	const topRatedItems = products.filter(
		(product) => product.category === "topRated"
	)

	useEffect(() => {
		fetchProducts()
	}, [])


	const topRated = products.filter(
    (product) => product.category === "topRated"
  )

  const featured = products.filter(
  	(product) => product.category === "featured"
	)


	return (
		<section className="shopping-list" id="products">
			<div className="container mt-4">
				<h2 className="mb-4">Our Products</h2>
				
				<Row>
					
					{ loading ? (
							<div className="loader">							
								<PropagateLoader color="#8f7840" />
							</div>
						) : (
							// products
						<>
							<div className="mb-3">
								<Button className="btn-categories btn-secondary" onClick={() => setValue("topRated")}>
									Top Rated
								</Button>
								<Button className="btn-categories btn-secondary" onClick={() => setValue("featured")}>
									Featured
								</Button>
								<Button className="btn-categories btn-secondary" onClick={() => setValue("all")}>
									All Products
								</Button>

								{value === "all" && <h4 className="mt-3 text-center">All Products</h4>}
								{value === "topRated" && <h4 className="mt-3 text-center">Top Rated</h4>}
								{value === "featured" && <h4 className="mt-3 text-center">Featured</h4>}
							</div>

							  {value === "all" &&
			          products.map((prod) => (
			            <Col key={prod._id} xs={12} md={6} lg={4}>
										<Item productProps={prod}/>
									</Col>	
			          ))}

			          {value === "topRated" &&
			          	topRatedItems.map((prod) => (
			            <Col key={prod._id} xs={12} md={6} lg={4}>
										<Item productProps={prod}/>
									</Col>	
			          ))}	

			          {value === "featured" &&
			          	featured.map((prod) => (
			            <Col key={prod._id} xs={12} md={6} lg={4}>
										<Item productProps={prod}/>
									</Col>	
			          ))}

						</>
						)				
					}
				</Row>
			</div>
			<Footer /> 
		</section>
	)
}

export default ShoppingList

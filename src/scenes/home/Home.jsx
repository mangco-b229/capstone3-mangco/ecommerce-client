import { Button } from "react-bootstrap"
import ShoppingList from "./ShoppingList"
import { useGlobalContext } from "../../state/index"
import { useEffect, useState } from "react"
import { useNavigate } from "react-router-dom"
import ProductSettings from "../admin/ProductSettings"

const Home = () => {
	const { user,showAdminDash, setShowAdminDash, isLoggedIn, setIsLoggedIn } = useGlobalContext()
	const [selectedPage, setSelectedPage, setCartCount] = useState("home")
	const navigate = useNavigate()

	useEffect(() => {
		if(user.isAdmin) {
			setShowAdminDash(true)
		} else {
			setShowAdminDash(false)
		}
	},[user])

	return (
		<div>
			
				{ isLoggedIn && showAdminDash ? (
					<ProductSettings />						
					) : (
						<div className="home">
							<section className="container hero-container d-flex align-items-center" id="home">
								<div className="hero">
									<h1>Summer Sale</h1>
									<p>High-quality kitchen knives for all needs. Efficient and enjoyable cooking experience. Durable materials.</p>
									<ul>
										<li>Free Shipping</li>
										<li>Wide Selection of Knives for All Cooking Needs</li>
										<li>Affordable Prices</li>
									</ul>
								</div>
							</section>
							<ShoppingList setSelectedPage={setSelectedPage}/>
						</div>
					)
				}
						
		</div>
	)
}

export default Home
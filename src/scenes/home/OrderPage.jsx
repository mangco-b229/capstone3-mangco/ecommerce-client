import { useEffect, useState } from "react"
import { Table } from "react-bootstrap"
import PropagateLoader from 'react-spinners/PropagateLoader'

const OrderPage = () => {
	const user = ({
		id: localStorage.getItem("id"),
		token: localStorage.getItem("token")
	})
	const [orders, setOrders] = useState([])

	const fetchOrders = async () => {
		const response = await fetch(`${import.meta.env.VITE_ECOM}/orders/user/${user.id}`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()
		setOrders(data)	
	}

	useEffect(() => {
		fetchOrders()
		console.log(user.token)
	},[])

	return (
		<div className="order-page">
			<div className="container">
				<h2 className="pt-4 mb-4">Your Orders</h2>
				{ orders ? (
						<Table striped bordered hover variant="dark" responsive>
				      <thead>
				        <tr>
				          <th>Order ID</th>
				          <th>Total Amount</th>
				          <th>Purhcased On</th>
				        </tr>
				      </thead>
				      <tbody>
				      	{ orders.map(order => (
									<tr key={order._id}>
										<td>{order._id}</td>
										<td>PhP {order.totalAmount}</td>
										<td className="text-center">
											{
				        				new Date(order.purhasedOn).toLocaleDateString("en-US", {
			        					 	year: "numeric",
										      month: "short",
										      day: "2-digit"
				        				})
				        			}
										</td>
									</tr>
								))}
				      </tbody>
			      </Table>
					) : (
						<div className="d-flex justify-content-center spinner" >
							<PropagateLoader color="#8f7840" />
						</div>
					)
				}					
			</div>
		</div>
	)
}

export default OrderPage
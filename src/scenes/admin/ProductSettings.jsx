
import { Button, Table, Form, DropdownButton, Dropdown } from "react-bootstrap"
import { useState, useEffect } from "react"
import { useGlobalContext } from "../../state/index"



const ProductSettings = () => {
	const { openModalProduct, products, setProducts, user, isUpdate, setIsUpdate , setSelectedProductId} = useGlobalContext()

	const fetchProducts = async () => {
		const res = await fetch(`${import.meta.env.VITE_ECOM}/products/all/admin`)
		const data = await res.json()

		setProducts(data)
	}

	const handleDropdownChange = async (productId, isActive) => {
	  const updatedProduct = await archiveProduct(productId, isActive)
	  
	  setProducts(prevProducts => prevProducts.map(product => {
	    if (product._id === productId) {
	      return { ...product, isActive: updatedProduct.isActive }
	    } else {
	      return product
	    }
	  }))
	}


	const archiveProduct = async (productId, isActive) => {
		const res = await fetch(`${import.meta.env.VITE_ECOM}/products/${productId}/archive`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ isActive })
		})

		const archivedProduct = await res.json()
		return archivedProduct
	}
	useEffect(() => {
		fetchProducts()
	}, [products])




	return (
		<div className="admin-bg">
			<div className="container table-container">
				<div className="d-flex flex-row w-100 justify-content-between">
					<h3 className="text-white">Product List</h3>
					<Button className="mb-4 admin-buttons btn-secondary btn-login" onClick={() => {openModalProduct(); setIsUpdate(false)}}>Create Product</Button>
				</div>
				<Table striped bordered hover variant="dark">
			      <thead>
			        <tr>
			          <th className="text-center">Product Name</th>
			          <th className="text-center">Description</th>
			          <th className="text-center">Category</th>
			          <th className="text-center">Price</th>
			          <th className="text-center">Archive</th>
			          <th className="text-center">Update</th>
			        </tr>
			      </thead>
			      <tbody>
			        {products.map(product => (
							  <tr key={product._id}>
							    <td>{product.name}</td>
							    <td>{product.description}</td>
							    <td>{product.category}</td>
							    <td className="text-center">{product.price}</td>
							    <td>
							      <DropdownButton
							        id="dropdown-basic-button"
							        className="btn-secondary"
							        title={product.isActive ? "Active" : "Archived"}
							      >
							        <Dropdown.Item
							          onClick={() => handleDropdownChange(product._id, true)}
							        >
							          Active
							        </Dropdown.Item>
							        <Dropdown.Item
							          onClick={() => handleDropdownChange(product._id, false)}
							        >
							          Archived
							        </Dropdown.Item>
							      </DropdownButton>
							    </td>
							    <td>
							      <Button
							        variant="btn btn-update btn-secondary"
							        onClick={() => {
							          setIsUpdate(true);
							          openModalProduct();
							          setSelectedProductId(product._id);
							        }}
							      >
							        Update
							      </Button>
							    </td>
							  </tr>
							))}
			      </tbody>
			    </Table>
			</div>
		</div>
	)
}

export default ProductSettings
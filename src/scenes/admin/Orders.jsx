import { useGlobalContext } from "../../state/index"
import { useState, useEffect } from "react"
import { Table } from "react-bootstrap"
import PropagateLoader from 'react-spinners/PropagateLoader'



const Orders = () => {
	const { user } = useGlobalContext()
	const [ orders, setOrders ] = useState([])
	const [loading, setLoading] = useState(false)

	const fetchOrders = async () => {
		setLoading(true)
		const response = await fetch (`${import.meta.env.VITE_ECOM}/orders/all`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()

		setOrders(data)
		setLoading(false)
	}

	useEffect(() => {
		fetchOrders()
	}, [])

	return (
		<div className="admin-bg">
			<div className="container table-container">
				<div className="w-100 text-white mb-3 admin-titles">
					<h3>Orders</h3>
				</div>

				{ loading ? 
					<div><PropagateLoader color="#8f7840" /></div>
					:
					<Table striped bordered hover variant="dark">
			      <thead>
			        <tr>
			          <th className="text-center">Order Id</th>
			          <th className="text-center">User Id</th>
			          <th className="text-center">Total Amount</th>
			          <th className="text-center">Purhcased On</th>
			        </tr>
			      </thead>
			      <tbody>
			        {orders.map(order => (
			        	<tr key={order._id}>
			        		<td>{order._id}</td>
			        		<td>{order.userId}</td>
			        		<td className="text-center">{order.totalAmount}</td>
			        		<td className="text-center">
			        			{
			        				new Date(order.purhasedOn).toLocaleDateString("en-US", {
		        					 	year: "numeric",
									      month: "short",
									      day: "2-digit"
			        				})
			        			}
			        		</td>
			        	</tr>
			        ))}
			      </tbody>
			    </Table>
				}
			</div>
		</div>
	)
}

export default Orders
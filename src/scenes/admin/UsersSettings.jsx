import { useGlobalContext } from "../../state/index"
import { useState, useEffect } from "react"
import { Table, DropdownButton, Form, Button } from "react-bootstrap"
import PropagateLoader from 'react-spinners/PropagateLoader'
import Swal from "sweetalert2"


const UsersSettings = () => {

	const { user } = useGlobalContext()
	const [ users, setUsers ] = useState([])
	const [loading, setLoading] = useState(false)

	const fetchUsers = async () => {
		setLoading(true)
		const response = await fetch(`${import.meta.env.VITE_ECOM}/users/${user.id}/all`, {
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})
		const data = await response.json()

		setUsers(data)
		setLoading(false)
	}

	const setAdmin = async (userId, isAdmin) => {
		const response = await fetch(`${import.meta.env.VITE_ECOM}/users/${userId}/setadmin`, {
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ isAdmin })
		})

		const data = await response.json()
		return data	
	}

	useEffect(() => {
		fetchUsers()
	}, [])

	const handleSetAdminClick = async (userId, isAdmin) => {
	  const updatedUser = await setAdmin(userId, isAdmin)
	  setUsers(prevUsers => prevUsers.map(user => {
	    if (user._id === userId) {
	      return { ...user, isAdmin: updatedUser.isAdmin }
	    } else {
	      return user
	    }
	  }))

	  Swal.fire({
			confirmButtonColor: '#8f7840',
			color: '#8f7840',
			background: '#ecebeb',
    	title: "User Updated",
    	icon: "success",
    	text: `User Credentials has been updated`
    })

    fetchUsers()
	}



	return (
		<div className="admin-bg">
			<div className="container table-container">							
			<div className="w-100 text-white mb-3 admin-titles">
				<h3>Users List</h3>
			</div>
			{ loading ? <div><PropagateLoader color="#8f7840" /></div>
				: 
				<Table striped bordered hover variant="dark">
			      <thead>
			        <tr>
			          <th className="text-center">User Id</th>
			          <th className="text-center">Email</th>
			          <th className="text-center">isAdmin</th>
			          <th className="text-center">Set Admin</th>
			        </tr>
			      </thead>
			      <tbody>
			        {users.map(user => (
							  <tr key={user._id}>
							    <td>{user._id}</td>
							    <td>{user.email}</td>
							    <td className="text-center">{user.isAdmin ? "true" : "false"}</td>
							    <td className="text-center">
							    	<Button
										  variant="btn btn-update btn-secondary"
										  onClick={() => handleSetAdminClick(user._id, !user.isAdmin)}
										>
										  {user.isAdmin ? "Set to Non-Admin" : "Set Admin"}
										</Button>
							    </td>
							  </tr>
							))}
			      </tbody>
			    </Table>
				}						
			</div>
		</div>
	)
}

export default UsersSettings
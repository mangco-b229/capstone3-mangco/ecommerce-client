import { useContext, useState, useEffect } from "react"
import { Button, Form, FloatingLabel } from "react-bootstrap"
import { useGlobalContext } from "../../state/index"
import Swal from "sweetalert2"


const CreateUpdateProductModal = () => {
	const [name, setName] = useState("")
	const [description, setDescription] = useState("")
	const [category, setCategory] = useState("topRated")
	const [price , setPrice] = useState()
	const { openModalProduct, user, closeModalProduct, isUpdate, setIsUpdate, selectedProductId } = useGlobalContext()

	const handleNameInput = (e) => setName(e.target.value)
	const handleDescInput = (e) => setDescription(e.target.value)
	const handlePriceInput = (e) => setPrice(e.target.value)
	const handleCategoryInput = (e) => setCategory(e.target.value)

	useEffect(() => {
		const fetchProduct = async () => {
			if(isUpdate) {
				const response = await fetch(`${import.meta.env.VITE_ECOM}/products/${selectedProductId}`, {
					method: "GET",
					headers: { 'Content-Type': 'application/json'}
				})
				const data = await response.json()
				setName(data.name)
				setDescription(data.description)
				setCategory(data.category)
				setPrice(data.price)
			} else {
				setName("")
				setDescription("")
				setPrice("")
			}
		}

		fetchProduct()
	}, [isUpdate, selectedProductId])

	const createProduct = async (e) => {
		e.preventDefault()

		const response = await fetch(`${import.meta.env.VITE_ECOM}/products/`, {
			method: "POST",
			body: JSON.stringify({ name, description, category, price }),
			headers: { 
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()
		
		Swal.fire({
			confirmButtonColor: '#8f7840',
			color: '#8f7840',
			background: '#ecebeb',
    	title: "Product Created",
    	icon: "success",
    	text: `Product ${name} has been successfully created!`
    })
    setName("")
		setDescription("")
		setPrice("")		
	}

	const updateProduct = async (e) => {
		e.preventDefault()

		const response = await fetch(`${import.meta.env.VITE_ECOM}/products/${selectedProductId}`, {
			method: "PUT", 
			body: JSON.stringify({ name, description, category, price}),
			headers: {
				Authorization: `Bearer ${user.token}`,
				'Content-Type': 'application/json'
			}
		})

		const data = await response.json()
		Swal.fire({
			confirmButtonColor: '#8f7840',
			color: '#8f7840',
			background: '#ecebeb',
    	title: "Product Updated",
    	icon: "success",
    	text: "Product has been successfully updated!"
    })
    setName("")
		setDescription("")
		setPrice("")
	}

	const handleCreateUpdateProduct = async (e) => {
		if(isUpdate) updateProduct(e)
		if(!isUpdate) createProduct(e)
		closeModalProduct() 
	}

	return (
		<aside className="modal-overlay-product">
			<div className="modal-container-product p-4">
				<h2 className="mb-3">{isUpdate ? "Update Product" : "Create Product"}</h2>
				<Form className="d-flex flex-column create-product" onSubmit={handleCreateUpdateProduct}>
					<Form.Group controlId="formBasicRegister">
						<FloatingLabel
							controlId="floatingName"
							label="Enter Product Name"
							className="mb-3"
						>							
							<Form.Control
								type="text"
								placeholder="Enter Product Name"
								name="name"
								value={name}
								onChange={handleNameInput}
								required
							/>
						</FloatingLabel>				
						
						<FloatingLabel
							controlId="floatingTextarea2"
							label="Enter Product Description"
							className="mb-3"
						>
							<Form.Control
								as="textarea"
								placeholder="Enter Product Description"
								name="description"
								style = {{ height: '130px'}}
								value={description}
								onChange={handleDescInput}
								required
							/>
						</FloatingLabel>

						<FloatingLabel
		 					controlId="floatingCategory"
		 					label="Select Category"
							className="mb-3"		 					
		 				>
							<Form.Control as="select" onChange={handleCategoryInput} value={category}>
								<option value="topRated">topRated</option>
								<option value="featured">featured</option>
							</Form.Control>
						</FloatingLabel>

						<FloatingLabel
							controlId="floatingnumberlabel"
							label="Enter Product Price"
						>
							<Form.Control
								type="number"
								placeholder="Enter Product Description"
								name="price"
								value={price}
								onChange={handlePriceInput}
								required
							/>
						</FloatingLabel>
					</Form.Group>
					<div className="buttons-container">
						<Button type="submit" className="btn-login btn-secondary">{isUpdate ? "Update Product" : "Create Product"}</Button>
						<Button 
							onClick={() => {
								closeModalProduct(); 
								setIsUpdate(false);
							}} 
							className="btn-danger"
						>Cancel</Button>
					</div>
				</Form>
			</div>
		</aside>
	)
}

export default CreateUpdateProductModal